import react.dom.render
import kotlin.browser.*

fun main()
{
	js("require('index.less');")
	js("require('typeface-lato');")
	
	window.onload = {
		render(document.getElementById("root")!!) {
			app()
		}
	}
}