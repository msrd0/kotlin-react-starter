import react.*
import react.dom.*

class App : RComponent<RProps, RState>()
{
	override fun RBuilder.render()
	{
		div("App") {
			h1 { +"IT WORKS" }
		}
	}
}

fun RBuilder.app() = child(App::class) {}
