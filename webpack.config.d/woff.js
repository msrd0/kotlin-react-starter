config.module.rules.push({
    test: /\.woff2?/,
    loader: 'url-loader',
    options: {
        limit: 2650,
        name: '[hash:8].[ext]'
    }
})
