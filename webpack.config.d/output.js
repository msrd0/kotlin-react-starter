const HtmlWebpackPlugin = require('html-webpack-plugin');

config.output.filename = '[hash:8].js'

config.plugins.push(new HtmlWebpackPlugin({
    inject: true,
    template: '../../../../public/index.html',
    minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
        useShortDoctype: true
    }
}))