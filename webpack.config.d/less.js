const MiniCssExtractPlugin = require('mini-css-extract-plugin');

config.resolve.modules.push("processedResources/Js/main");

config.module.rules.push({
    test: /\.(le|c)ss/,
    use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        'less-loader'
    ]
})

config.plugins.push(new MiniCssExtractPlugin({
    filename: '[contenthash:8].css',
    chunkFilename: '[contenthash:8].chunk.css'
}))
