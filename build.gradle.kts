import org.jetbrains.kotlin.gradle.dsl.KotlinJsCompile
import org.jetbrains.kotlin.gradle.dsl.KotlinJsProjectExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinJsPluginWrapper

plugins {
	kotlin("js") version "1.3.72"
	id("com.github.ben-manes.versions") version "0.29.0"
}

repositories {
	jcenter()
	maven("https://kotlin.bintray.com/kotlinx")
	maven("https://kotlin.bintray.com/kotlin-js-wrappers")
}

kotlin {
	sourceSets["main"].kotlin.srcDir("src/")
	sourceSets["main"].resources.srcDir("public/")
}

plugins.withType<KotlinJsPluginWrapper> {
	extensions.configure<KotlinJsProjectExtension> {
		target {
			browser ()
		}
	}
	
	dependencies {
		val htmlVersion = "0.7.1"
		val reactVersion = "16.13.0-pre.95-kotlin-1.3.72"
		
		implementation(kotlin("stdlib-js"))
		implementation("org.jetbrains.kotlinx:kotlinx-html-js:$htmlVersion")
		implementation("org.jetbrains:kotlin-react:$reactVersion")
		implementation("org.jetbrains:kotlin-react-dom:$reactVersion")
		
		implementation(npm("react", "16.13.0"))
		implementation(npm("react-dom", "16.13.0"))
		implementation(npm("typeface-lato"))
		
		// TODO these should be dev-dependencies
		implementation(npm("css-loader"))
		implementation(npm("file-loader"))
		implementation(npm("html-webpack-plugin"))
		implementation(npm("less"))
		implementation(npm("less-loader"))
		implementation(npm("mini-css-extract-plugin"))
		implementation(npm("url-loader"))
	}
	
	tasks.withType<KotlinJsCompile>().configureEach {
		kotlinOptions {
			moduleKind = "commonjs"
			
			if (name == "compileKotlinJs") {
				sourceMapEmbedSources = "always"
				sourceMap = true
			} else {
				sourceMap = false
			}
		}
	}
}
